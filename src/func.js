const getSum = (str1, str2) => {
  if (typeof str1 !== "string" || typeof str2 !== "string") return false;

  if (Number.isNaN(Number(str1)) || Number.isNaN(Number(str2))) return false;

  return String(Number(str1) + Number(str2));  
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postsCount = 0;
  let commentsCount = 0;

  listOfPosts.forEach(post => {
    if (post.author === authorName) {
      postsCount++;
    }
    if (post.hasOwnProperty("comments")) {
      post.comments.forEach(comment => {
        if(comment.author === authorName) {
          commentsCount++;
        }
      })
    }
    
  });
  
  return `Post:${postsCount},comments:${commentsCount}`
};

const tickets=(people)=> {
  let cashInRegister = 0;
  let ticketPrice = 25;

  for(let person of people) {
    if (person == 25) {
      cashInRegister += 25;
    } else if (person - ticketPrice <= cashInRegister) {
      cashInRegister = cashInRegister + person - ticketPrice;
    } else return 'NO'
  }

  return 'YES'
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};